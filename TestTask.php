<?php
/**
 * Created by PhpStorm.
 * User: greka
 * Date: 04.07.16
 */

namespace TestTask;


class TestTask
{
    public $sign_on, // дата входа.
        $sign_off;  // data выхода.
    private $aDate,     //  не сортированный массив данных
        $oDate,      // объект DateTime
        $oNewDate,   //  Объект времени для клонирования.DateTime
        $bDebug = false; // включить выключить отладочный режим.
    const COUNT_USER = 100;


    /**
     * при создании имитируем поступление данных из бд.
     */
    public function __construct()
    {
        $this->initDate();
    }

    /**
     * Метод заполняет данными о посетителях  массив
     */
    private function initDate()
    {
        for ($i = 0; $i < self::COUNT_USER; $i++) {
            $this->aDate[] = $this->getDate();
        }
    }

    /**
     * генерируем имитацию данных о времени посещения.
     */
    private function getDate()
    {
        $oTmpDate = [];
        if (empty($this->oNewDate)) {
            $this->oNewDate = new \DateTime();
        }
        $this->oDate = clone $this->oNewDate;

        $oTmpDate["on"] = $this->oDate->setTime($this->getRand(), 0)->format('Y-m-d H:i:s');
        $oTmpDate["off"] = $this->oDate->add(new \DateInterval('PT' . $this->getRand(24) . 'H' . $this->getRand() . "M" . $this->getRand() . "S"))->format('Y-m-d H:i:s');
        return $oTmpDate;
    }

    /**
     * Получение случайного число от 1 до $max для сдвига по часам минутам и сек  в передлах суток.
     *
     * @param int $max
     *
     * @return int
     */
    private function getRand($max = 60)
    {
        return rand(1, $max);
    }

    /**
     *  метод вычисляет и отображает статистичные данные.
     */
    private function getHighLoad()
    {
        $aTime = [];
        //       проходим список пользователей
        foreach ($this->aDate as $oDateUser) {
            $iStartDate = strtotime($oDateUser['on']);
            $iStartFinish = strtotime($oDateUser['off']);
            foreach ($this->aDate as $oDateIntersection) {
                $iDateIntersectionFinish = strtotime($oDateIntersection['off']);
                if ($iDateIntersectionFinish >= $iStartDate && $iDateIntersectionFinish <= $iStartFinish) { // ищем людей у которых дата выхода более даты старта текущего пользователя и менее даты выхода текущего пользователя.
                    $sKeyName = $oDateUser['on'] . '|' . $oDateUser['off'];
                    if(isset($aTime[$sKeyName])){
                        $sKeyName .= "|" . $oDateUser['on'];
                    }
                    $aTime[$sKeyName]++;
                }
            }
        }
        // отладочный вывод массивов времени и пересечений.
        if ($this->bDebug) {
            echo "<pre>";
            echo "<p> ========================================== массив пересечений ============================";
            print_r($aTime);
            echo "<p> ========================================== массив данных =================================";
            print_r($this->aDate);
            echo "</pre>";
        }
        $this->viewInterval($aTime);
    }

    /**
     * @param $aHour
     * отображение списка часов с пометкой самых загруженных часов.
     */
    public function viewInterval($aTime)
    {
        arsort($aTime);
        reset($aTime);
        $aMaxTime = explode('|', key($aTime));
        $aTime = array_shift($aTime);

        echo "кол-во посетителей всего за сутки " . self::COUNT_USER;
        echo "<p>Наибольшее количество посетителей (" . $aTime . ") было с " . $aMaxTime[0] . "по  $aMaxTime[1].";
        die;
    }

    /** запуск */
    public function  run()
    {
        $this->getHighLoad();
    }
}

?>
